from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

# Replace 'YOUR_BOT_TOKEN' with the token provided by BotFather
BOT_TOKEN = '6616831077:AAFFdSDiry_GJwnTaQI8wbhe4-2frpizxbs'
CHANNEL_USERNAME = '@bom_bom_test'

def start(update, context):
    # Create buttons for joining the channel and checking subscription
    join_channel_button = InlineKeyboardButton("👉 Kanalga a'zo bolish", url=f"https://t.me/bom_bom_test")
    check_subscription_button = InlineKeyboardButton("🔍 Tekshirish ", callback_data='check')

    # Create a keyboard with the two buttons
    reply_markup = InlineKeyboardMarkup([
        [join_channel_button],
        [check_subscription_button]
    ])

    # Send a message with the buttons
    update.message.reply_text("Assalomu alaykum! Kanalga a'zo bolganingizni bilish uchun tekshirish tugmasini bosing 😊👇", reply_markup=reply_markup)

def check_subscription(update, context):
    query = update.callback_query
    user_id = query.from_user.id

    try:
        # Check if the user is a member of the channel
        chat_member = context.bot.get_chat_member(CHANNEL_USERNAME, user_id)

        if chat_member.status in ['member', 'administrator', 'creator']:
            query.edit_message_text("✅ Siz kanalga a'zo bo'lgansiz 😊!")
        else:
            # Check if the user has clicked the button before
            if context.user_data.get('check_button_clicked', False):
                # Send a new message with the updated buttons
                join_channel_button = InlineKeyboardButton("👉 Kanalga a'zo bolish", url=f"https://t.me/bom_bom_test")
                check_subscription_button = InlineKeyboardButton("🔍 Tekshirish ", callback_data='check')

                reply_markup = InlineKeyboardMarkup([
                    [join_channel_button],
                    [check_subscription_button]
                ])

                context.bot.send_message(chat_id=query.message.chat_id, text="❌ Siz kanalga a'zo bo'lmagansiz. Iltimos, Kanalga a'zo bo'lish tugmasini bosing 👇", reply_markup=reply_markup)
            else:
                # Update the existing message to show that the user has not joined
                join_channel_button = InlineKeyboardButton("👉 Kanalga a'zo bolish", url=f"https://t.me/bom_bom_test")
                check_subscription_button = InlineKeyboardButton("🔍 Tekshirish ", callback_data='check')

                reply_markup = InlineKeyboardMarkup([
                    [join_channel_button],
                    [check_subscription_button]
                ])

                query.edit_message_text("❌ Siz kanalga a'zo bo'lmagansiz. Iltimos, Kanalga a'zo bo'lish tugmasini bosing 👇", reply_markup=reply_markup)

                # Set a flag in user_data indicating that the button has been clicked
                context.user_data['check_button_clicked'] = True
    except Exception as e:
        if "CHAT_ADMIN_REQUIRED" in str(e):
            query.edit_message_text("⚠️ This bot needs to be an administrator in the channel to check subscriptions.")
        else:
            query.edit_message_text(f"❌ Error: {e}")

def main():
    updater = Updater(token=BOT_TOKEN, use_context=True)
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CallbackQueryHandler(check_subscription, pattern='check'))

    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    main()
